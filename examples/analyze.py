import MeCab

from sentiment_ja.analyzer import SentimentAnalyzerJa


tagger = MeCab.Tagger("-Owakati")
text = "日本だったら機械学習イキり系の人が日本オワタと馬鹿騒ぎするところだけどサンフランシスコはどうかな"
words = tagger.parse(text).split(' ')

analyzer = SentimentAnalyzerJa()
print(text)
print(analyzer.analyze(words))
