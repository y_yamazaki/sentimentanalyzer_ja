import codecs
import os
import requests

import pandas as pd

__all__ = ['SentimentAnalyzerJa']


class SentimentAnalyzerJa:

    def __init__(self, path='pn_ja.dic', download=True):
        self.url = 'http://www.lr.pi.titech.ac.jp/~takamura/pubs/pn_ja.dic'
        self.path = path
        dict_exists = self._check_dict_exists()
        if (not dict_exists) and download:
            self._download_dict()

        self.word2score = self._make_word2sentimentscore(path)
        self.word2score['ない'] = -1

    def analyze(self, words):
        score = 0
        for word in words:
            if word in self.word2score:
                score += self.word2score[word]
        return score / (len(words) + 1e-8)

    def _check_dict_exists(self):
        return True if os.path.exists(self.path) else False

    def _download_dict(self):
        print(f'Downloading from {self.url}, to {self.path}')
        r = requests.get(self.url, allow_redirects=True)
        with open(self.path, 'wb') as f:
            f.write(r.content)

    def _make_word2sentimentscore(self, path):
        with codecs.open('pn_ja.dic', 'r', 'Shift-JIS', 'ignore') as file:
            df = pd.read_table(file, delimiter=":", header=None)
        df.columns = ['word', 'word_kana', 'parts_of_speech', 'score']
        dic = {}
        for i in range(len(df)):
            keys = (df['word'].loc[i], df['word_kana'].loc[i])
            for key in keys:
                dic[key] = df['score'].loc[i]
        return dic
