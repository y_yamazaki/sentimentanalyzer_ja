# sentimentanalyzer_ja
[極性辞書](http://www.lr.pi.titech.ac.jp/~takamura/pndic_ja.html)を用いたexpert systemによる日本語感情分析ツール  
一応研究用でしか使用出来ないらしい  
[こちらの辞書](http://www.cl.ecei.tohoku.ac.jp/index.php?Open%20Resources%2FJapanese%20Sentiment%20Polarity%20Dictionary)なら商用利用も可能

## Installation
```shell
git clone https://github.com/yutayamazaki/sentimentanalyzer_ja
cd sentimentanalyzer_ja
python setup.py develop
```

## Example

```python
import MeCab

from sentiment_ja.analyzer import SentimentAnalyzerJa


tagger = MeCab.Tagger("-Owakati")
text = "日本だったら機械学習イキり系の人が日本オワタと馬鹿騒ぎするところだけどサンフランシスコはどうかな"
words = tagger.parse(text).split(' ')

analyzer = SentimentAnalyzerJa()
print(text)
print(analyzer.analyze(words))
```

```shell
日本だったら機械学習イキり系の人が日本オワタと馬鹿騒ぎするところだけどサンフランシスコはどうかな
-0.4503016798198793
```
